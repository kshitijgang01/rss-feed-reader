package cron

import (
	"context"
	b64 "encoding/base64"
	"log"
	"time"

	"encoding/json"

	"cloud.google.com/go/pubsub"
	"github.com/go-co-op/gocron"
	"github.com/mmcdole/gofeed"
)

var scheduler *gocron.Scheduler
var client *pubsub.Client
var topic *pubsub.Topic

type Article struct {
	Title       string    `json:"title"`
	Link        string    `json:"link"`
	Guid        string    `json:"guid"`
	Id          string    `json:"id"`
	PubDate     time.Time `json:"pubdate"`
	Description string    `json:"description"`
}

var projectID string = "audio-news-354718"
var topicID string = "article-review"

func Init() {
	var err error

	scheduler = gocron.NewScheduler(time.UTC)
	scheduler.StartAsync()
	ctx := context.Background()

	client, err = pubsub.NewClient(ctx, projectID)
	topic = client.Topic(topicID)
	if err != nil {
		log.Fatal(err)
	}
}

func gcloudPub(a []byte) error {
	ctx := context.Background()
	result := topic.Publish(ctx, &pubsub.Message{
		Data: a,
	})
	id, err := result.Get(ctx)
	if err != nil {
		return err
	}
	log.Println("[pub-sub] published message to topic ", "article-review ", id)
	return nil

}

func task(rssFeedUrl string, timePeriod int) {
	fp := gofeed.NewParser()
	feed, _ := fp.ParseURL(rssFeedUrl)
	allArticles := feed.Items
	for _, val := range allArticles {
		if time.Now().Add(-time.Hour * time.Duration(timePeriod)).UTC().Before(*val.PublishedParsed) {
			article := Article{
				Title:       val.Title,
				Link:        val.Link,
				Guid:        val.GUID,
				Id:          b64.StdEncoding.EncodeToString([]byte(val.Link + "$$" + val.GUID)),
				PubDate:     *val.PublishedParsed,
				Description: val.Description,
			}
			articeMarshalled, err := json.Marshal(article)
			if err != nil {
				log.Println(err)
				continue
			}
			err = gcloudPub(articeMarshalled)
			if err != nil {
				log.Println(err)
				continue
			}

		} else {
			break
		}
	}

}

//main-service@audio-news-354718.iam.gserviceaccount.com
func AddTask(tag []string, rssFeedUrl string, timePeriod int) {

	scheduler.Every(timePeriod).Hours().Tag(tag...).Do(task, rssFeedUrl, timePeriod)

}

func RemoveTask(tag []string) {
	scheduler.RemoveByTags(tag...)
}
